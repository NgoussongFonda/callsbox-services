package com.commeduc.dev.callsboxServices.models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO wihtout any XML configuration and also provide the Spring 
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class CardDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the user in the database.
   */
  public void create(Card card) {
    entityManager.persist(card);
    return;
  }
  
  /**
   * Delete the user from the database.
   */
  public void delete(Card card) {
    if (entityManager.contains(card))
      entityManager.remove(card);
    else
      entityManager.remove(entityManager.merge(card));
    return;
  }
  
  /**
   * Return all the users stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<Card> getAll() {
    return entityManager.createQuery("from Card").getResultList();
  }
  
  /**
   * Return the user having the passed email.
     * @param code
     * @return 
   */
  public Card getByCode(String code) {
    return (Card) entityManager.createQuery(
        "FROM Card WHERE code LIKE :code")
        .setParameter("code", code)
        .getSingleResult();
  }

  /**
   * Return the user having the passed id.
     * @param id
     * @return 
   */
  public Card getById(long id) {
    return entityManager.find(Card.class, id);
  }

  /**
   * Update the passed user in the database.
     * @param card
   */
  public void update(Card card) {
    entityManager.merge(card);
    return;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
