/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.common;

/**
 *
 * @author deugueu
 */
public class ProviderNumbersUtils {
    
    
    public static String getProvider(String number){
        if( number.startsWith("655") || number.startsWith("656") 
                || number.startsWith("657") || number.startsWith("658") 
                || number.startsWith("659") || number.startsWith("69")){
            return "OCM";
        }
        else {
            if( number.startsWith("650") || number.startsWith("651") 
                || number.startsWith("652") || number.startsWith("653") 
                || number.startsWith("654") || number.startsWith("67")){
            return "MTN";
            }
            else {
                if( number.startsWith("66") ){
                    return "NEX";
                }
                else {
                    if( number.startsWith("2") ){
                        return "CMT";
                    }
                    else {
                        return "OTH";
                    }
                }
            }
        }
    }
    
}
