package com.commeduc.dev.callsboxServices.models;

import com.commeduc.dev.callsboxServices.domain.ResourcesLine;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO wihtout any XML configuration and also provide the Spring 
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class ResourcesLineDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the user in the database.
     * @param account
   */
  public void create(ResourcesLine account) {
    entityManager.persist(account);
    return;
  }
  
  /**
   * Delete the user from the database.
     * @param account
   */
  public void delete(ResourcesLine account) {
    if (entityManager.contains(account))
      entityManager.remove(account);
    else
      entityManager.remove(entityManager.merge(account));
    return;
  }
  
  /**
   * Return all the users stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<ResourcesLine> getAll() {
    return entityManager.createQuery("from ResourcesLine").getResultList();
  }
  
  /**
   * Return the user having the passed msisdn.
     * @param trunk
     * @param provider
     * @return 
   */
  public ResourcesLine getByTrunkSupAndHplmn(String trunk, String provider) {
      
    return (ResourcesLine) entityManager.createQuery(
        "from ResourcesLine where RES_TRUNC_SUP = :RES_TRUNC_SUP AND RES_HPLMN = :RES_HPLMN")
        .setParameter("RES_TRUNC_SUP", trunk)
        .setParameter("RES_HPLMN", provider)
        .getSingleResult();
  }

  /**
   * Update the passed user in the database.
     * @param account
   */
  public void update(ResourcesLine account) {
    entityManager.merge(account);
    return;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
