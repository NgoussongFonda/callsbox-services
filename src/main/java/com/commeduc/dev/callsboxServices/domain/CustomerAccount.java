/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deugueu
 */
@Entity
@Table(name = "cust_account")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomerAccount.findAll", query = "SELECT c FROM CustomerAccount c"),
    @NamedQuery(name = "CustomerAccount.findByCptCustId", query = "SELECT c FROM CustomerAccount c WHERE c.cptCustId = :cptCustId"),
    @NamedQuery(name = "CustomerAccount.findByCptDiIsdn", query = "SELECT c FROM CustomerAccount c WHERE c.cptDiIsdn = :cptDiIsdn"),
    @NamedQuery(name = "CustomerAccount.findByCptIsdn", query = "SELECT c FROM CustomerAccount c WHERE c.cptIsdn = :cptIsdn"),
    @NamedQuery(name = "CustomerAccount.findByCptIsdn1", query = "SELECT c FROM CustomerAccount c WHERE c.cptIsdn1 = :cptIsdn1"),
    @NamedQuery(name = "CustomerAccount.findByCptIsdn2", query = "SELECT c FROM CustomerAccount c WHERE c.cptIsdn2 = :cptIsdn2"),
    @NamedQuery(name = "CustomerAccount.findByCptIsdn3", query = "SELECT c FROM CustomerAccount c WHERE c.cptIsdn3 = :cptIsdn3"),
    @NamedQuery(name = "CustomerAccount.findByCptType", query = "SELECT c FROM CustomerAccount c WHERE c.cptType = :cptType"),
    @NamedQuery(name = "CustomerAccount.findByCptMainAcc", query = "SELECT c FROM CustomerAccount c WHERE c.cptMainAcc = :cptMainAcc"),
    @NamedQuery(name = "CustomerAccount.findByCptMainCag", query = "SELECT c FROM CustomerAccount c WHERE c.cptMainCag = :cptMainCag"),
    @NamedQuery(name = "CustomerAccount.findByCptSellDay", query = "SELECT c FROM CustomerAccount c WHERE c.cptSellDay = :cptSellDay"),
    @NamedQuery(name = "CustomerAccount.findByCptDateActivation", query = "SELECT c FROM CustomerAccount c WHERE c.cptDateActivation = :cptDateActivation"),
    @NamedQuery(name = "CustomerAccount.findByCptLastTransDatetime", query = "SELECT c FROM CustomerAccount c WHERE c.cptLastTransDatetime = :cptLastTransDatetime"),
    @NamedQuery(name = "CustomerAccount.findByCptDateProchDepot", query = "SELECT c FROM CustomerAccount c WHERE c.cptDateProchDepot = :cptDateProchDepot"),
    @NamedQuery(name = "CustomerAccount.findByCptLast201Datetime", query = "SELECT c FROM CustomerAccount c WHERE c.cptLast201Datetime = :cptLast201Datetime"),
    @NamedQuery(name = "CustomerAccount.findByCptLast201Limit", query = "SELECT c FROM CustomerAccount c WHERE c.cptLast201Limit = :cptLast201Limit"),
    @NamedQuery(name = "CustomerAccount.findByCptPas", query = "SELECT c FROM CustomerAccount c WHERE c.cptPas = :cptPas"),
    @NamedQuery(name = "CustomerAccount.findByCptForfaitMo", query = "SELECT c FROM CustomerAccount c WHERE c.cptForfaitMo = :cptForfaitMo"),
    @NamedQuery(name = "CustomerAccount.findByCptForfaitCbk", query = "SELECT c FROM CustomerAccount c WHERE c.cptForfaitCbk = :cptForfaitCbk"),
    @NamedQuery(name = "CustomerAccount.findByCptForfaitCall", query = "SELECT c FROM CustomerAccount c WHERE c.cptForfaitCall = :cptForfaitCall"),
    @NamedQuery(name = "CustomerAccount.findByCptForfaitBlksms", query = "SELECT c FROM CustomerAccount c WHERE c.cptForfaitBlksms = :cptForfaitBlksms"),
    @NamedQuery(name = "CustomerAccount.findByCptDeact", query = "SELECT c FROM CustomerAccount c WHERE c.cptDeact = :cptDeact"),
    @NamedQuery(name = "CustomerAccount.findByCptLock", query = "SELECT c FROM CustomerAccount c WHERE c.cptLock = :cptLock"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilMo", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilMo = :cptRefilMo"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilAuto", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilAuto = :cptRefilAuto"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilCall", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilCall = :cptRefilCall"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilEmo", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilEmo = :cptRefilEmo"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilEmt", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilEmt = :cptRefilEmt"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilBlk", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilBlk = :cptRefilBlk"),
    @NamedQuery(name = "CustomerAccount.findByCptRefilCbk", query = "SELECT c FROM CustomerAccount c WHERE c.cptRefilCbk = :cptRefilCbk"),
    @NamedQuery(name = "CustomerAccount.findByCptPwd", query = "SELECT c FROM CustomerAccount c WHERE c.cptPwd = :cptPwd"),
    @NamedQuery(name = "CustomerAccount.findByCptGlobalCa", query = "SELECT c FROM CustomerAccount c WHERE c.cptGlobalCa = :cptGlobalCa"),
    @NamedQuery(name = "CustomerAccount.findByCptGlobalSell", query = "SELECT c FROM CustomerAccount c WHERE c.cptGlobalSell = :cptGlobalSell"),
    @NamedQuery(name = "CustomerAccount.findByCptCaMonth", query = "SELECT c FROM CustomerAccount c WHERE c.cptCaMonth = :cptCaMonth"),
    @NamedQuery(name = "CustomerAccount.findByCptCaSellMonth", query = "SELECT c FROM CustomerAccount c WHERE c.cptCaSellMonth = :cptCaSellMonth"),
    @NamedQuery(name = "CustomerAccount.findByCptCaution", query = "SELECT c FROM CustomerAccount c WHERE c.cptCaution = :cptCaution"),
    @NamedQuery(name = "CustomerAccount.findByCptCredit", query = "SELECT c FROM CustomerAccount c WHERE c.cptCredit = :cptCredit"),
    @NamedQuery(name = "CustomerAccount.findByCptRemb", query = "SELECT c FROM CustomerAccount c WHERE c.cptRemb = :cptRemb"),
    @NamedQuery(name = "CustomerAccount.findByCptBnsSms", query = "SELECT c FROM CustomerAccount c WHERE c.cptBnsSms = :cptBnsSms"),
    @NamedQuery(name = "CustomerAccount.findByCptCommission", query = "SELECT c FROM CustomerAccount c WHERE c.cptCommission = :cptCommission"),
    @NamedQuery(name = "CustomerAccount.findByCptMainTmp", query = "SELECT c FROM CustomerAccount c WHERE c.cptMainTmp = :cptMainTmp"),
    @NamedQuery(name = "CustomerAccount.findByCptLastReceiver", query = "SELECT c FROM CustomerAccount c WHERE c.cptLastReceiver = :cptLastReceiver"),
    @NamedQuery(name = "CustomerAccount.findByCptLastReceiverCount", query = "SELECT c FROM CustomerAccount c WHERE c.cptLastReceiverCount = :cptLastReceiverCount"),
    @NamedQuery(name = "CustomerAccount.findByCptLastReceiverAmount", query = "SELECT c FROM CustomerAccount c WHERE c.cptLastReceiverAmount = :cptLastReceiverAmount"),
    @NamedQuery(name = "CustomerAccount.findByCptNetwkOcm", query = "SELECT c FROM CustomerAccount c WHERE c.cptNetwkOcm = :cptNetwkOcm"),
    @NamedQuery(name = "CustomerAccount.findByCptNetwkMtn", query = "SELECT c FROM CustomerAccount c WHERE c.cptNetwkMtn = :cptNetwkMtn"),
    @NamedQuery(name = "CustomerAccount.findByCptNetwkNex", query = "SELECT c FROM CustomerAccount c WHERE c.cptNetwkNex = :cptNetwkNex"),
    @NamedQuery(name = "CustomerAccount.findByCptNetwkCmt", query = "SELECT c FROM CustomerAccount c WHERE c.cptNetwkCmt = :cptNetwkCmt"),
    @NamedQuery(name = "CustomerAccount.findByCptAccountRule", query = "SELECT c FROM CustomerAccount c WHERE c.cptAccountRule = :cptAccountRule"),
    @NamedQuery(name = "CustomerAccount.findByCptAccountStatus", query = "SELECT c FROM CustomerAccount c WHERE c.cptAccountStatus = :cptAccountStatus"),
    @NamedQuery(name = "CustomerAccount.findByCptDelayDepot", query = "SELECT c FROM CustomerAccount c WHERE c.cptDelayDepot = :cptDelayDepot")})
    
public class CustomerAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CPT_CUST_ID")
    private String cptCustId;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "CPT_DI_ISDN")
    private String cptDiIsdn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CPT_ISDN")
    private String cptIsdn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CPT_ISDN_1")
    private String cptIsdn1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CPT_ISDN_2")
    private String cptIsdn2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CPT_ISDN_3")
    private String cptIsdn3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "CPT_TYPE")
    private String cptType;
   
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_MAIN_ACC")
    private BigDecimal cptMainAcc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_MAIN_CAG")
    private int cptMainCag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_SELL_DAY")
    private int cptSellDay;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "CPT_DATE_ACTIVATION")
    private String cptDateActivation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "CPT_LAST_TRANS_DATETIME")
    private String cptLastTransDatetime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "CPT_DATE_PROCH_DEPOT")
    private String cptDateProchDepot;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CPT_LAST_201_DATETIME")
    private String cptLast201Datetime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_LAST_201_LIMIT")
    private int cptLast201Limit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_PAS")
    private int cptPas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_FORFAIT_MO")
    private int cptForfaitMo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_FORFAIT_CBK")
    private int cptForfaitCbk;
    @Column(name = "CPT_FORFAIT_CALL")
    private Integer cptForfaitCall;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_FORFAIT_BLKSMS")
    private int cptForfaitBlksms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "CPT_DEACT")
    private String cptDeact;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "CPT_LOCK")
    private String cptLock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_MO")
    private boolean cptRefilMo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_AUTO")
    private boolean cptRefilAuto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_CALL")
    private boolean cptRefilCall;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_EMO")
    private boolean cptRefilEmo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_EMT")
    private boolean cptRefilEmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_BLK")
    private boolean cptRefilBlk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REFIL_CBK")
    private boolean cptRefilCbk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "CPT_PWD")
    private String cptPwd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_GLOBAL_CA")
    private int cptGlobalCa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_GLOBAL_SELL")
    private int cptGlobalSell;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_CA_MONTH")
    private int cptCaMonth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_CA_SELL_MONTH")
    private int cptCaSellMonth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_CAUTION")
    private int cptCaution;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_CREDIT")
    private boolean cptCredit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_REMB")
    private BigDecimal cptRemb;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_BNS_SMS")
    private int cptBnsSms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_COMMISSION")
    private BigDecimal cptCommission;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_MAIN_TMP")
    private int cptMainTmp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "CPT_LAST_RECEIVER")
    private String cptLastReceiver;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_LAST_RECEIVER_COUNT")
    private int cptLastReceiverCount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_LAST_RECEIVER_AMOUNT")
    private int cptLastReceiverAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_NETWK_OCM")
    private int cptNetwkOcm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_NETWK_MTN")
    private int cptNetwkMtn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_NETWK_NEX")
    private int cptNetwkNex;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_NETWK_CMT")
    private int cptNetwkCmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "CPT_ACCOUNT_RULE")
    private String cptAccountRule;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CPT_ACCOUNT_STATUS")
    private String cptAccountStatus;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CPT_DELAY_DEPOT")
    private int cptDelayDepot;

    public CustomerAccount() {
    }

    public CustomerAccount(String cptDiIsdn) {
        this.cptDiIsdn = cptDiIsdn;
    }

    public CustomerAccount(String cptDiIsdn, String cptCustId, String cptIsdn, String cptIsdn1, String cptIsdn2, String cptIsdn3, String cptType, BigDecimal cptMainAcc, int cptMainCag, int cptSellDay, String cptDateActivation, String cptLastTransDatetime, String cptDateProchDepot, String cptLast201Datetime, int cptLast201Limit, int cptPas, int cptForfaitMo, int cptForfaitCbk, int cptForfaitBlksms, String cptDeact, String cptLock, boolean cptRefilMo, boolean cptRefilAuto, boolean cptRefilCall, boolean cptRefilEmo, boolean cptRefilEmt, boolean cptRefilBlk, boolean cptRefilCbk, String cptPwd, int cptGlobalCa, int cptGlobalSell, int cptCaMonth, int cptCaSellMonth, int cptCaution, boolean cptCredit, BigDecimal cptRemb, int cptBnsSms, BigDecimal cptCommission, int cptMainTmp, String cptLastReceiver, int cptLastReceiverCount, int cptLastReceiverAmount, int cptNetwkOcm, int cptNetwkMtn, int cptNetwkNex, int cptNetwkCmt, String cptAccountRule, String cptAccountStatus, int cptDelayDepot, int cpt801, int cpt802, int cptBns100, BigDecimal cptBns101, BigDecimal cptBns102, BigDecimal cptBns200, BigDecimal cptBns201, BigDecimal cptBns300, BigDecimal cptBns301, BigDecimal cptBns500, BigDecimal cptBns501, BigDecimal cptBns801, BigDecimal cptBns802, int cptCa, String cptLast300Receiver, int cptLast300ReceiverAmount, int cptLast300ReceiverCount, int cptMain100, int cptMain101, int cptMain102, int cptMain200, int cptMain201, int cptMain300, int cptMain301, int cptMain500, int cptMain501, int cptMain801, int cptMain802, String cptMasterNetw, BigDecimal cptNetworkMain, BigDecimal cptNetworkSell, BigDecimal cptPrv101, boolean cptServCbk, boolean cptServDde, boolean cptServEmo, short cptServEmt, String cptUsername, String cptUsersubname, int netwkCmt, int netwkMtn, int netwkNex, int netwkOcm) 
    {
        this.cptDiIsdn = cptDiIsdn;
        this.cptCustId = cptCustId;
        this.cptIsdn = cptIsdn;
        this.cptIsdn1 = cptIsdn1;
        this.cptIsdn2 = cptIsdn2;
        this.cptIsdn3 = cptIsdn3;
        this.cptType = cptType;
        this.cptMainAcc = cptMainAcc;
        this.cptMainCag = cptMainCag;
        this.cptSellDay = cptSellDay;
        this.cptDateActivation = cptDateActivation;
        this.cptLastTransDatetime = cptLastTransDatetime;
        this.cptDateProchDepot = cptDateProchDepot;
        this.cptLast201Datetime = cptLast201Datetime;
        this.cptLast201Limit = cptLast201Limit;
        this.cptPas = cptPas;
        this.cptForfaitMo = cptForfaitMo;
        this.cptForfaitCbk = cptForfaitCbk;
        this.cptForfaitBlksms = cptForfaitBlksms;
        this.cptDeact = cptDeact;
        this.cptLock = cptLock;
        this.cptRefilMo = cptRefilMo;
        this.cptRefilAuto = cptRefilAuto;
        this.cptRefilCall = cptRefilCall;
        this.cptRefilEmo = cptRefilEmo;
        this.cptRefilEmt = cptRefilEmt;
        this.cptRefilBlk = cptRefilBlk;
        this.cptRefilCbk = cptRefilCbk;
        this.cptPwd = cptPwd;
        this.cptGlobalCa = cptGlobalCa;
        this.cptGlobalSell = cptGlobalSell;
        this.cptCaMonth = cptCaMonth;
        this.cptCaSellMonth = cptCaSellMonth;
        this.cptCaution = cptCaution;
        this.cptCredit = cptCredit;
        this.cptRemb = cptRemb;
        this.cptBnsSms = cptBnsSms;
        this.cptCommission = cptCommission;
        this.cptMainTmp = cptMainTmp;
        this.cptLastReceiver = cptLastReceiver;
        this.cptLastReceiverCount = cptLastReceiverCount;
        this.cptLastReceiverAmount = cptLastReceiverAmount;
        this.cptNetwkOcm = cptNetwkOcm;
        this.cptNetwkMtn = cptNetwkMtn;
        this.cptNetwkNex = cptNetwkNex;
        this.cptNetwkCmt = cptNetwkCmt;
        this.cptAccountRule = cptAccountRule;
        this.cptAccountStatus = cptAccountStatus;
        this.cptDelayDepot = cptDelayDepot;
       
    }

    public String getCptCustId() {
        return cptCustId;
    }

    public void setCptCustId(String cptCustId) {
        this.cptCustId = cptCustId;
    }

    public String getCptDiIsdn() {
        return cptDiIsdn;
    }

    public void setCptDiIsdn(String cptDiIsdn) {
        this.cptDiIsdn = cptDiIsdn;
    }

    public String getCptIsdn() {
        return cptIsdn;
    }

    public void setCptIsdn(String cptIsdn) {
        this.cptIsdn = cptIsdn;
    }

    public String getCptIsdn1() {
        return cptIsdn1;
    }

    public void setCptIsdn1(String cptIsdn1) {
        this.cptIsdn1 = cptIsdn1;
    }

    public String getCptIsdn2() {
        return cptIsdn2;
    }

    public void setCptIsdn2(String cptIsdn2) {
        this.cptIsdn2 = cptIsdn2;
    }

    public String getCptIsdn3() {
        return cptIsdn3;
    }

    public void setCptIsdn3(String cptIsdn3) {
        this.cptIsdn3 = cptIsdn3;
    }

    public String getCptType() {
        return cptType;
    }

    public void setCptType(String cptType) {
        this.cptType = cptType;
    }

    public BigDecimal getCptMainAcc() {
        return cptMainAcc;
    }

    public void setCptMainAcc(BigDecimal cptMainAcc) {
        this.cptMainAcc = cptMainAcc;
    }

    public int getCptMainCag() {
        return cptMainCag;
    }

    public void setCptMainCag(int cptMainCag) {
        this.cptMainCag = cptMainCag;
    }

    public int getCptSellDay() {
        return cptSellDay;
    }

    public void setCptSellDay(int cptSellDay) {
        this.cptSellDay = cptSellDay;
    }

    public String getCptDateActivation() {
        return cptDateActivation;
    }

    public void setCptDateActivation(String cptDateActivation) {
        this.cptDateActivation = cptDateActivation;
    }

    public String getCptLastTransDatetime() {
        return cptLastTransDatetime;
    }

    public void setCptLastTransDatetime(String cptLastTransDatetime) {
        this.cptLastTransDatetime = cptLastTransDatetime;
    }

    public String getCptDateProchDepot() {
        return cptDateProchDepot;
    }

    public void setCptDateProchDepot(String cptDateProchDepot) {
        this.cptDateProchDepot = cptDateProchDepot;
    }

    public String getCptLast201Datetime() {
        return cptLast201Datetime;
    }

    public void setCptLast201Datetime(String cptLast201Datetime) {
        this.cptLast201Datetime = cptLast201Datetime;
    }

    public int getCptLast201Limit() {
        return cptLast201Limit;
    }

    public void setCptLast201Limit(int cptLast201Limit) {
        this.cptLast201Limit = cptLast201Limit;
    }

    public int getCptPas() {
        return cptPas;
    }

    public void setCptPas(int cptPas) {
        this.cptPas = cptPas;
    }

    public int getCptForfaitMo() {
        return cptForfaitMo;
    }

    public void setCptForfaitMo(int cptForfaitMo) {
        this.cptForfaitMo = cptForfaitMo;
    }

    public int getCptForfaitCbk() {
        return cptForfaitCbk;
    }

    public void setCptForfaitCbk(int cptForfaitCbk) {
        this.cptForfaitCbk = cptForfaitCbk;
    }

    public Integer getCptForfaitCall() {
        return cptForfaitCall;
    }

    public void setCptForfaitCall(Integer cptForfaitCall) {
        this.cptForfaitCall = cptForfaitCall;
    }

    public int getCptForfaitBlksms() {
        return cptForfaitBlksms;
    }

    public void setCptForfaitBlksms(int cptForfaitBlksms) {
        this.cptForfaitBlksms = cptForfaitBlksms;
    }

    public String getCptDeact() {
        return cptDeact;
    }

    public void setCptDeact(String cptDeact) {
        this.cptDeact = cptDeact;
    }

    public String getCptLock() {
        return cptLock;
    }

    public void setCptLock(String cptLock) {
        this.cptLock = cptLock;
    }

    public boolean getCptRefilMo() {
        return cptRefilMo;
    }

    public void setCptRefilMo(boolean cptRefilMo) {
        this.cptRefilMo = cptRefilMo;
    }

    public boolean getCptRefilAuto() {
        return cptRefilAuto;
    }

    public void setCptRefilAuto(boolean cptRefilAuto) {
        this.cptRefilAuto = cptRefilAuto;
    }

    public boolean getCptRefilCall() {
        return cptRefilCall;
    }

    public void setCptRefilCall(boolean cptRefilCall) {
        this.cptRefilCall = cptRefilCall;
    }

    public boolean getCptRefilEmo() {
        return cptRefilEmo;
    }

    public void setCptRefilEmo(boolean cptRefilEmo) {
        this.cptRefilEmo = cptRefilEmo;
    }

    public boolean getCptRefilEmt() {
        return cptRefilEmt;
    }

    public void setCptRefilEmt(boolean cptRefilEmt) {
        this.cptRefilEmt = cptRefilEmt;
    }

    public boolean getCptRefilBlk() {
        return cptRefilBlk;
    }

    public void setCptRefilBlk(boolean cptRefilBlk) {
        this.cptRefilBlk = cptRefilBlk;
    }

    public boolean getCptRefilCbk() {
        return cptRefilCbk;
    }

    public void setCptRefilCbk(boolean cptRefilCbk) {
        this.cptRefilCbk = cptRefilCbk;
    }

    public String getCptPwd() {
        return cptPwd;
    }

    public void setCptPwd(String cptPwd) {
        this.cptPwd = cptPwd;
    }

    public int getCptGlobalCa() {
        return cptGlobalCa;
    }

    public void setCptGlobalCa(int cptGlobalCa) {
        this.cptGlobalCa = cptGlobalCa;
    }

    public int getCptGlobalSell() {
        return cptGlobalSell;
    }

    public void setCptGlobalSell(int cptGlobalSell) {
        this.cptGlobalSell = cptGlobalSell;
    }

    public int getCptCaMonth() {
        return cptCaMonth;
    }

    public void setCptCaMonth(int cptCaMonth) {
        this.cptCaMonth = cptCaMonth;
    }

    public int getCptCaSellMonth() {
        return cptCaSellMonth;
    }

    public void setCptCaSellMonth(int cptCaSellMonth) {
        this.cptCaSellMonth = cptCaSellMonth;
    }

    public int getCptCaution() {
        return cptCaution;
    }

    public void setCptCaution(int cptCaution) {
        this.cptCaution = cptCaution;
    }

    public boolean getCptCredit() {
        return cptCredit;
    }

    public void setCptCredit(boolean cptCredit) {
        this.cptCredit = cptCredit;
    }

    public BigDecimal getCptRemb() {
        return cptRemb;
    }

    public void setCptRemb(BigDecimal cptRemb) {
        this.cptRemb = cptRemb;
    }

    public int getCptBnsSms() {
        return cptBnsSms;
    }

    public void setCptBnsSms(int cptBnsSms) {
        this.cptBnsSms = cptBnsSms;
    }

    public BigDecimal getCptCommission() {
        return cptCommission;
    }

    public void setCptCommission(BigDecimal cptCommission) {
        this.cptCommission = cptCommission;
    }

    public int getCptMainTmp() {
        return cptMainTmp;
    }

    public void setCptMainTmp(int cptMainTmp) {
        this.cptMainTmp = cptMainTmp;
    }

    public String getCptLastReceiver() {
        return cptLastReceiver;
    }

    public void setCptLastReceiver(String cptLastReceiver) {
        this.cptLastReceiver = cptLastReceiver;
    }

    public int getCptLastReceiverCount() {
        return cptLastReceiverCount;
    }

    public void setCptLastReceiverCount(int cptLastReceiverCount) {
        this.cptLastReceiverCount = cptLastReceiverCount;
    }

    public int getCptLastReceiverAmount() {
        return cptLastReceiverAmount;
    }

    public void setCptLastReceiverAmount(int cptLastReceiverAmount) {
        this.cptLastReceiverAmount = cptLastReceiverAmount;
    }

    public int getCptNetwkOcm() {
        return cptNetwkOcm;
    }

    public void setCptNetwkOcm(int cptNetwkOcm) {
        this.cptNetwkOcm = cptNetwkOcm;
    }

    public int getCptNetwkMtn() {
        return cptNetwkMtn;
    }

    public void setCptNetwkMtn(int cptNetwkMtn) {
        this.cptNetwkMtn = cptNetwkMtn;
    }

    public int getCptNetwkNex() {
        return cptNetwkNex;
    }

    public void setCptNetwkNex(int cptNetwkNex) {
        this.cptNetwkNex = cptNetwkNex;
    }

    public int getCptNetwkCmt() {
        return cptNetwkCmt;
    }

    public void setCptNetwkCmt(int cptNetwkCmt) {
        this.cptNetwkCmt = cptNetwkCmt;
    }

    public String getCptAccountRule() {
        return cptAccountRule;
    }

    public void setCptAccountRule(String cptAccountRule) {
        this.cptAccountRule = cptAccountRule;
    }

    public String getCptAccountStatus() {
        return cptAccountStatus;
    }

    public void setCptAccountStatus(String cptAccountStatus) {
        this.cptAccountStatus = cptAccountStatus;
    }

    public int getCptDelayDepot() {
        return cptDelayDepot;
    }

    public void setCptDelayDepot(int cptDelayDepot) {
        this.cptDelayDepot = cptDelayDepot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cptDiIsdn != null ? cptDiIsdn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerAccount)) {
            return false;
        }
        CustomerAccount other = (CustomerAccount) object;
        if ((this.cptDiIsdn == null && other.cptDiIsdn != null) || (this.cptDiIsdn != null && !this.cptDiIsdn.equals(other.cptDiIsdn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.callsbox.domain.CustomerAccount[ cptDiIsdn=" + cptDiIsdn + " ]";
    }

    public void getCptMainAcc(BigDecimal bigDecimal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
