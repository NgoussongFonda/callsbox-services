/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.common;

import com.commeduc.dev.callsboxServices.domain.CustomerAccount;

/**
 *
 * @author deugueu
 */
public class SharedCustomerAccount {
    
    private String id;
    private String msisdn, password, type, profile;
    private double credit;
    private boolean valid;
            
    public SharedCustomerAccount(CustomerAccount cAccount, boolean valid){
        
        this.id = cAccount.getCptCustId();
        //this.id = 100;
        
        this.msisdn = cAccount.getCptIsdn();
        this.password = cAccount.getCptPwd();
        this.type = cAccount.getCptType();
        
        if( cAccount.getCptMainAcc() != null ){
            this.credit = cAccount.getCptMainAcc().doubleValue();
        }
        else {
            this.credit = 0;
        }
        this.valid = valid;
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
    
    
    
    
    
}
