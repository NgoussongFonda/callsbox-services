package com.commeduc.dev.callsboxServices.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Represents an User for this web application.
 */
@Entity
@Table(name = "callsbox_card")
public class Card {

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  
  @NotNull
  private String code;
  
  @NotNull
  private double amount;
  
  @NotNull
  private Date expirationDate;

  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  public Card() { }

  public Card(long id) { 
    this.id = id;
  }

  public Card(String code, double amount) {
    this.code = code;
    this.amount = amount;
  }
  
  public Card(String code, double amount, Date expirationDate) {
    this.code = code;
    this.amount = amount;
    this.expirationDate = expirationDate;
  }

  public long getId() {
    return id;
  }

  public void setId(long value) {
    this.id = value;
  }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "Card{" + "id=" + id + ", code=" + code + ", amount=" + amount + ", expirationDate=" + expirationDate + '}';
    }

    
 
  
} // class Card
