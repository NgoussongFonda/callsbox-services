/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.controllers;
import com.commeduc.dev.callsboxServices.models.CustomerAccountDao;
import com.commeduc.dev.callsboxServices.models.ResourcesLineDao;
import com.commeduc.dev.callsboxServices.common.ProviderNumbersUtils;
import com.commeduc.dev.callsboxServices.common.SharedOperationResponse;
import com.commeduc.dev.callsboxServices.common.StringConsts;
import com.commeduc.dev.callsboxServices.domain.AutorizedDomain;
import com.commeduc.dev.callsboxServices.domain.CustomerAccount;
import com.commeduc.dev.callsboxServices.models.AutorizedDomainDao;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author deugueu
 */
@Controller
public class CallsBoxService_AllwithDraw_WebController 
{
    // Wire the UserDao used inside this controller.
    @Autowired
    private ResourcesLineDao resourcesLineDao;
    // Wire the UserDao used inside this controller.
    @Autowired
    private CustomerAccountDao custumerAccountDao;
    @Autowired
    private AutorizedDomainDao autorizedDomainDao;


    /**
     * @param UsingService
     * @param BasicService
     * @param id_provider
     * @param Msisd_provider
     * @param pwd_code
     * @param imei
     * @param imsi
     * @param receiver_msisdn
     * @param Amount
     * @param callbacklocation
     * @return
     */

    @RequestMapping(value="/callsbox/WebServices/allwithdraw/{UsingService}/{BasicService}/{id_provider}/{receiver_msisdn}/{Amount/{callback}/{id_transaction}", method = {RequestMethod.GET},
        /*consumes = MediaType.APPLICATION_XML_VALUE,*/
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public SharedOperationResponse CallsBoxServiceWebRequest(
            @PathVariable("UsingService") int UsingService,
            @PathVariable("BasicService") int BasicService,
            @PathVariable(value="id_provider") String id_provider,
            @PathVariable(value="pwd_code") String pwd_code,
            @PathVariable(value="receiver_msisdn") String receiver_msisdn,
            @PathVariable(value="Amount") String Amount,
            @PathVariable(value="callbacklocation") String callbacklocation,
            @PathVariable(value="id_transaction") String id_transaction,HttpServletRequest request ) 
    {
            //String ip = InetAddress.getLocalHost().getHostAddress();
            String hostname = request.getRemoteAddr(); 
            String code = id_provider + hostname; //"kaolo.commeduc.com";
            String sha256hexCodeReceived = DigestUtils.md5DigestAsHex(code.getBytes());
            // Vérifier la validité du compte du service web émetteur de la requete 
             AutorizedDomain ParrainAccount = autorizedDomainDao.getByAdDomainAccount(id_provider);
            String sha256hexRealkeyCode =ParrainAccount.getAutorizedDomainPK().getAdDomainAccount();
            
            // Vérification en MD5 du mot de passe saisi 
            if(!sha256hexRealkeyCode.equals(sha256hexCodeReceived)){                   
            return new SharedOperationResponse(false,false,-1, "Le site inconnu.");
            }
            
            CustomerAccount SenderAccount = custumerAccountDao.getAccountByCptCustId(id_provider);
            if(!SenderAccount.getCptAccountStatus().equals(0)){                   
                return new SharedOperationResponse(false,false,-1, "Ce service n'est pas encore actif sur ce site.");
            }

            String Msisd_provider=SenderAccount.getCptIsdn();
            String imei= "", imsi= "";
            
            String typeCommand = "" ;
            String ServiceSelected = "" ;
            ServiceSelected="SERVICE_"+BasicService;
            if(""+UsingService+""+BasicService == "StringConsts."+ServiceSelected)
                {
                    typeCommand = "I"+UsingService+"*S"+BasicService;
                }
                else 
                {
                    return new SharedOperationResponse(false,false,-1, "Le Service sollicité n'existe pas. Merci de vérifier vos paramètres");
                }
            try 
                {
                    String provider = ProviderNumbersUtils.getProvider(receiver_msisdn);

                 // Preparation à l'exécution de la demande
                    Date currenTime = Calendar.getInstance().getTime();
                    DateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
                    File deposit = new File(StringConsts.FILE_DEPOSIT);
                    File requestFile = new File(deposit, "CDRAPK"+Msisd_provider+""+format.format(currenTime)+".dat" );
                    String executionCommand = "";
                    executionCommand = MainController.getWithdrawWebExecutionCommand(Msisd_provider, receiver_msisdn, Amount, imei, id_transaction, callbacklocation, typeCommand, requestFile);
                    try 
                    {
                        Path created = Files.createFile(requestFile.toPath() );
                        Files.write(
                                Paths.get(requestFile.toURI()), 
                                executionCommand.getBytes("utf-8"), 
                                StandardOpenOption.CREATE, 
                                StandardOpenOption.TRUNCATE_EXISTING);
                                Logger.getLogger(CallsBoxService_AllwithDraw_WebController.class.getName()).log(Level.INFO, "File created and stored : "+created.toString(), created);
                    } catch (IOException ex) {
                        Logger.getLogger(CallsBoxService_AllwithDraw_WebController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return new SharedOperationResponse(true, true, 18000, "Votre Demande a été prise en Compte.Vous allez recevoir un SMS de confirmation");
            }
                catch(org.springframework.dao.EmptyResultDataAccessException | javax.persistence.NoResultException nre){
                return new SharedOperationResponse(false,false,-10, "No user found with this username.");
            }
                catch (Exception ex) {
                ex.printStackTrace();
            }return new SharedOperationResponse(false,false,-10, "Something went wrong. Retry");
    }

}
