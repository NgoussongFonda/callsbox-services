package com.commeduc.dev.callsboxServices.controllers;

import com.commeduc.dev.callsboxServices.common.StringConsts;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

  @RequestMapping("/")
  @ResponseBody
  public String index() 
  {
    return "Proudly handcrafted by " + "<a href='https://www.cbxm.cm/'>COMMEDUC @237</a> :)";
  }
  
  /**
   * 
   * @param account
   * @param code
   * @param profile
   * @param imei
   * @param imsi
   * @param location
   * @param typeCommand
   * @param requestFile
   * @return 
   */
  public static String getAccountRegistrationCommand( String Sender, String account, String code, String profile,
      String imei, String imsi, String location, String typeCommand, File requestFile ){
      Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");             
      // I200*S200*CodeSecret*Destinataire*Montant
                String command = typeCommand + "*"+ code.trim() + "*"+ account.trim() + "*"+ profile.trim();
                String executionCommand = format.format(currenTime)+ ";"
                + Sender + ";"
                + command + ";"
                + "+237694500600;0;APK;APK 1.0;IP169.86;ETR;APK;"
                + imsi + ";"
                + imei + ";" // Double semicolon
                + "FR" + ";" // TODO Language from sender
                + location + ";" 
                + "APK" + ";"
                + requestFile.getName();
                return executionCommand;
  }
   public static String getAccountWebRegistrationCommand( String Sender,String account, String code, String profile,String imei, String imsi, String location, String typeCommand, File requestFile )
   { 
    Date currenTime = Calendar.getInstance().getTime();
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    String command = typeCommand + "*"+ code.trim() + "*"+ account.trim() + "*"+ profile.trim();
    String executionCommand = format.format(currenTime)+ ";"
        + Sender + ";"
        + command + ";"
        + "+237694500600;0;WEB;WEB 1.0;IP169.86;ETR;WEB;"
        + imsi + ";"
        + imei + ";" // Double semicolon
        + "FR" + ";" // TODO Language from sender
        + location + ";" 
        + "WEB" + ";"
        + requestFile.getName();
    return executionCommand;
  }
  
  /**
   * 
   * @param account
   * @param code
   * @param imei
   * @param imsi
   * @param location
   * @param conversionType
   * @param destination
   * @param amount
   * @param typeCommand
   * @param requestFile
   * @return 
   */
    public static String getTopUpWebExecutionCommand( String Sender,String code, String Receiver, String amount, String imei, String imsi, String location, String typeCommand, File requestFile )
    {
        Date currenTime = Calendar.getInstance().getTime();
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    
      // I200*S200*CodeSecret*Destinataire*Montant
        String command = typeCommand 
            + "*"+ code
            + "*"+ Receiver.trim()
            + "*"+ amount.trim();
      
            String executionCommand = format.format(currenTime)+ ";"
                + Sender + ";"
                + command + ";"
                + "+237694500600;0;WEB;WEB 1.0;IP169.86;ETR;WEB;"
                + imsi + ";"
                + imei + ";" // Double semicolon
                + "FR" + ";" // TODO Language from sender
                + location + ";" 
                + "WEB" + ";"
                + requestFile.getName();
            return executionCommand;
  }
  public static String getTopUpExecutionCommand( String account, String code, 
          String imei, String imsi, String location, 
          String conversionType,
          String destination, String amount,
          String typeCommand, File requestFile ){
      
      Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    
      // I200*S200*CodeSecret*Destinataire*Montant
        String command = typeCommand 
            + "*"+ code
            + "*"+ destination.trim()
            + "*"+ amount.trim();
      
            String executionCommand = format.format(currenTime)+ ";"
                                  + account + ";"
                                  + command + ";"
                                  + "+237694500600;0;APK;APK 1.0;IP169.86;ETR;APK;"
                                  + imsi + ";"
                                  + imei + ";" // Double semicolon
                                  + "FR" + ";" // TODO Language from sender
                                  + location + ";" 
                                  + "APK" + ";"
                                  + requestFile.getName();
            return executionCommand;
  }
   public static String IdentificationExecutionCommand( String Sender,String account, String Code,String imei, String imsi, String location,String typeCommand, File requestFile )
   { Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");               
      // I200*S200*CodeSecret*Destinataire*Montant
        String command = typeCommand
            + "*"+ account.trim();
      
            String executionCommand = format.format(currenTime)+ ";"
                                  + Sender + ";"
                                  + command + ";"
                                  + "+237694500600;0;APK;APK 1.0;IP169.86;ETR;APK;"
                                  + imsi + ";"
                                  + imei + ";" // Double semicolon
                                  + "FR" + ";" // TODO Language from sender
                                  + location + ";" 
                                  + "APK" + ";"
                                  + requestFile.getName();
            return executionCommand;
  }
  public static String getWithdrawWebMarchandExecutionCommand( String id_customer_marchand, String msisdn, 
          String imei, String imsi, String callback, 
          int share,
          String id_transaction, String amount,
          String typeCommand, File requestFile ){
      
      Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    
      // I200*S200*CodeSecret*Destinataire*Montant
                    StringBuilder command = new StringBuilder(typeCommand); // "I200*S200"
                            
                    command.append("*"+ id_customer_marchand.trim()
                            + "*"+ msisdn.trim()
                            + "*"+ amount.trim()
                            + "*"+ share                           
                            + "*"+ id_transaction.trim()
                    );
      
      String executionCommand = format.format(currenTime)+ ";"
                            + id_customer_marchand + ";"
                            + command.toString() + ";"
                             + "+237694500600;0;APK;APK 1.0;IP169.86;ETR;APK;"
                            + imsi + ";"
                            + imei + ";" // Double semicolon
                            + "FR" + ";" // TODO Language from sender
                            + callback + ";" 
                            + "APK" + ";"
                            + requestFile.getName();                           
      return executionCommand;
  }

  /**
   * 
   * @param account
   * @param code
   * @param imei
   * @param imsi
   * @param location
   * @param operationType
   * @param destination
   * @param amount
   * @param typeCommand
   * @param requestFile
   * @return 
   */
  
  public static String getWithdrawExecutionCommand( String Sender, String Receiver,String amount, String imei, String imsi, String location,  String typeCommand, File requestFile )
  {
      
      Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    
      // I200*S200*CodeSecret*Destinataire*Montant
        StringBuilder command = new StringBuilder(typeCommand); 

        command.append(
                "*"+ Receiver.trim()
                + "*"+ amount.trim() 
        );

            String executionCommand = format.format(currenTime)+ ";"
                                  + Sender + ";"
                                  + command + ";"
                                  + "+237694500600;0;APK;APK 1.0;IP169.86;ETR;APK;"
                                  + imsi + ";"
                                  + imei + ";" // Double semicolon
                                  + "FR" + ";" // TODO Language from sender
                                  + location + ";" 
                                  + "APK" + ";"
                                  + requestFile.getName();
            return executionCommand;
  }
  public static String getWithdrawWebExecutionCommand( String Sender, String Receiver,String amount, String imei, String imsi, String location,  String typeCommand, File requestFile )
  { Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    
      // I200*S200*CodeSecret*Destinataire*Montant
        StringBuilder command = new StringBuilder(typeCommand); 

        command.append(
                "*"+ Receiver.trim()
                + "*"+ amount.trim() 
        );

            String executionCommand = format.format(currenTime)+ ";"
                                  + Sender + ";"
                                  + command + ";"
                                  + "+237694500600;0;WEB;WEB 1.0;IP169.86;ETR;WEB;"
                                  + imsi + ";"
                                  + imei + ";" // Double semicolon
                                  + "FR" + ";" // TODO Language from sender
                                  + location + ";" 
                                  + "WEB" + ";"
                                  + requestFile.getName();
            return executionCommand;
  }
  public static String getConversionExecutionCommand( String Sender, String account,String amount, String imei, String imsi, String location,  String typeCommand, File requestFile )
  {
    Date currenTime = Calendar.getInstance().getTime();
      DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        StringBuilder command = new StringBuilder(typeCommand); 
        command.append(
                "*"+ account.trim()
                + "*"+ amount.trim() 
        );
        String executionCommand = format.format(currenTime)+ ";"
            + Sender + ";"
            + command + ";"
            + "+237694500600;0;APK;APK 1.0;IP169.86;ETR;APK;"
            + imsi + ";"
            + imei + ";" // Double semicolon
            + "FR" + ";" // TODO Language from sender
            + location + ";" 
            + "APK" + ";"
            + requestFile.getName();
        return executionCommand;
  }
}
