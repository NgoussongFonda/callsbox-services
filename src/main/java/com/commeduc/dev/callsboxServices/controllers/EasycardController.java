package com.commeduc.dev.callsboxServices.controllers;

import com.commeduc.dev.callsboxServices.models.Card;
import com.commeduc.dev.callsboxServices.models.CardDao;
import com.commeduc.dev.callsboxServices.domain.Easycard;
import com.commeduc.dev.callsboxServices.models.EasycardDao;
import com.commeduc.dev.callsboxServices.models.User;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * Class UserController
 */
@Controller
public class EasycardController {

  // ------------------------
  // PUBLIC METHODS
  // ------------------------

    /**
   * Retrieve the id for the card with the passed code.
     * @param serial
     * @param code
     * @return 
   */
  @RequestMapping(value="/getcard-by-serial", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Easycard getBySerial(String serial) {
    try {
      Easycard card = easycardDao.getBySerial(serial);
      return card;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
  /**
   * Retrieve the id for the card with the passed code.
     * @param code
     * @return 
   */
  @RequestMapping(value="/getcard-by-code", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Easycard getByCode(String code) {
      
    try {
      Easycard card = easycardDao.getByCode(code);
      return card;
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }
    
    return null;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // Wire the UserDao used inside this controller.
  @Autowired
  private EasycardDao easycardDao;
  
} // class UserController
