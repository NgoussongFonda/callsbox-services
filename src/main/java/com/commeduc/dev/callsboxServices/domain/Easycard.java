/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deugueu
 */
@Entity
@Table(name = "easycard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Easycard.findAll", query = "SELECT e FROM Easycard e"),
    @NamedQuery(name = "Easycard.findByECSerial", query = "SELECT e FROM Easycard e WHERE e.eCSerial = :eCSerial"),
    @NamedQuery(name = "Easycard.findByECPwD", query = "SELECT e FROM Easycard e WHERE e.eCPwD = :eCPwD"),
    @NamedQuery(name = "Easycard.findByECFacialValue", query = "SELECT e FROM Easycard e WHERE e.eCFacialValue = :eCFacialValue"),
    @NamedQuery(name = "Easycard.findByECInitailValue", query = "SELECT e FROM Easycard e WHERE e.eCInitailValue = :eCInitailValue"),
    @NamedQuery(name = "Easycard.findByECValue", query = "SELECT e FROM Easycard e WHERE e.eCValue = :eCValue"),
    @NamedQuery(name = "Easycard.findByECUsed", query = "SELECT e FROM Easycard e WHERE e.eCUsed = :eCUsed"),
    @NamedQuery(name = "Easycard.findByECUsedDate", query = "SELECT e FROM Easycard e WHERE e.eCUsedDate = :eCUsedDate"),
    @NamedQuery(name = "Easycard.findByECFlagActivation", query = "SELECT e FROM Easycard e WHERE e.eCFlagActivation = :eCFlagActivation"),
    @NamedQuery(name = "Easycard.findByECActivDate", query = "SELECT e FROM Easycard e WHERE e.eCActivDate = :eCActivDate"),
    @NamedQuery(name = "Easycard.findByEcTypeZone", query = "SELECT e FROM Easycard e WHERE e.ecTypeZone = :ecTypeZone"),
    @NamedQuery(name = "Easycard.findByECFirstUsedcustomer", query = "SELECT e FROM Easycard e WHERE e.eCFirstUsedcustomer = :eCFirstUsedcustomer"),
    @NamedQuery(name = "Easycard.findByECCardlost", query = "SELECT e FROM Easycard e WHERE e.eCCardlost = :eCCardlost"),
    @NamedQuery(name = "Easycard.findByECDealerMaster", query = "SELECT e FROM Easycard e WHERE e.eCDealerMaster = :eCDealerMaster"),
    @NamedQuery(name = "Easycard.findByEcMasterNetw", query = "SELECT e FROM Easycard e WHERE e.ecMasterNetw = :ecMasterNetw"),
    @NamedQuery(name = "Easycard.findByEcService", query = "SELECT e FROM Easycard e WHERE e.ecService = :ecService")})
public class Easycard implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EC_Serial")
    private String eCSerial;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EC_PwD")
    private String eCPwD;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EC_Facial_Value")
    private double eCFacialValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EC_Initail_Value")
    private double eCInitailValue;
    @Column(name = "EC_Value")
    private Double eCValue;
    @Column(name = "EC_Used")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eCUsed;
    @Column(name = "EC_UsedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eCUsedDate;
    @Column(name = "EC_Flag_Activation")
    private Boolean eCFlagActivation;
    @Column(name = "EC_Activ_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eCActivDate;
    @Size(max = 5)
    @Column(name = "EC_TYPE_ZONE")
    private String ecTypeZone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "EC_First_Used_customer")
    private String eCFirstUsedcustomer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EC_Card_lost")
    private short eCCardlost;
    @Size(max = 9)
    @Column(name = "EC_Dealer_Master")
    private String eCDealerMaster;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 22)
    @Column(name = "EC_MASTER_NETW")
    private String ecMasterNetw;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "EC_SERVICE")
    private String ecService;

    public Easycard() {
    }

    public Easycard(String eCSerial) {
        this.eCSerial = eCSerial;
    }

    public Easycard(String eCSerial, String eCPwD, double eCFacialValue, double eCInitailValue, String eCFirstUsedcustomer, short eCCardlost, String ecMasterNetw, String ecService) {
        this.eCSerial = eCSerial;
        this.eCPwD = eCPwD;
        this.eCFacialValue = eCFacialValue;
        this.eCInitailValue = eCInitailValue;
        this.eCFirstUsedcustomer = eCFirstUsedcustomer;
        this.eCCardlost = eCCardlost;
        this.ecMasterNetw = ecMasterNetw;
        this.ecService = ecService;
    }

    public String getECSerial() {
        return eCSerial;
    }

    public void setECSerial(String eCSerial) {
        this.eCSerial = eCSerial;
    }

    public String getECPwD() {
        return eCPwD;
    }

    public void setECPwD(String eCPwD) {
        this.eCPwD = eCPwD;
    }

    public double getECFacialValue() {
        return eCFacialValue;
    }

    public void setECFacialValue(double eCFacialValue) {
        this.eCFacialValue = eCFacialValue;
    }

    public double getECInitailValue() {
        return eCInitailValue;
    }

    public void setECInitailValue(double eCInitailValue) {
        this.eCInitailValue = eCInitailValue;
    }

    public Double getECValue() {
        return eCValue;
    }

    public void setECValue(Double eCValue) {
        this.eCValue = eCValue;
    }

    public Date getECUsed() {
        return eCUsed;
    }

    public void setECUsed(Date eCUsed) {
        this.eCUsed = eCUsed;
    }

    public Date getECUsedDate() {
        return eCUsedDate;
    }

    public void setECUsedDate(Date eCUsedDate) {
        this.eCUsedDate = eCUsedDate;
    }

    public Boolean getECFlagActivation() {
        return eCFlagActivation;
    }

    public void setECFlagActivation(Boolean eCFlagActivation) {
        this.eCFlagActivation = eCFlagActivation;
    }

    public Date getECActivDate() {
        return eCActivDate;
    }

    public void setECActivDate(Date eCActivDate) {
        this.eCActivDate = eCActivDate;
    }

    public String getEcTypeZone() {
        return ecTypeZone;
    }

    public void setEcTypeZone(String ecTypeZone) {
        this.ecTypeZone = ecTypeZone;
    }

    public String getECFirstUsedcustomer() {
        return eCFirstUsedcustomer;
    }

    public void setECFirstUsedcustomer(String eCFirstUsedcustomer) {
        this.eCFirstUsedcustomer = eCFirstUsedcustomer;
    }

    public short getECCardlost() {
        return eCCardlost;
    }

    public void setECCardlost(short eCCardlost) {
        this.eCCardlost = eCCardlost;
    }

    public String getECDealerMaster() {
        return eCDealerMaster;
    }

    public void setECDealerMaster(String eCDealerMaster) {
        this.eCDealerMaster = eCDealerMaster;
    }

    public String getEcMasterNetw() {
        return ecMasterNetw;
    }

    public void setEcMasterNetw(String ecMasterNetw) {
        this.ecMasterNetw = ecMasterNetw;
    }

    public String getEcService() {
        return ecService;
    }

    public void setEcService(String ecService) {
        this.ecService = ecService;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eCSerial != null ? eCSerial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Easycard)) {
            return false;
        }
        Easycard other = (Easycard) object;
        if ((this.eCSerial == null && other.eCSerial != null) || (this.eCSerial != null && !this.eCSerial.equals(other.eCSerial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.callsbox.card.models.Easycard[ eCSerial=" + eCSerial + " ]";
    }
    
}
