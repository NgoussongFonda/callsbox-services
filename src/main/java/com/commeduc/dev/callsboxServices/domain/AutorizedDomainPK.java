/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fonda
 */
@Embeddable
public class AutorizedDomainPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "AD_DOMAIN_KEY")
    private String adDomainKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "AD_DOMAIN_ACCOUNT")
    private String adDomainAccount;

    public AutorizedDomainPK() {
    }

    public AutorizedDomainPK(String adDomainKey, String adDomainAccount) {
        this.adDomainKey = adDomainKey;
        this.adDomainAccount = adDomainAccount;
    }

    public String getAdDomainKey() {
        return adDomainKey;
    }

    public void setAdDomainKey(String adDomainKey) {
        this.adDomainKey = adDomainKey;
    }

    public String getAdDomainAccount() {
        return adDomainAccount;
    }

    public void setAdDomainAccount(String adDomainAccount) {
        this.adDomainAccount = adDomainAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adDomainKey != null ? adDomainKey.hashCode() : 0);
        hash += (adDomainAccount != null ? adDomainAccount.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutorizedDomainPK)) {
            return false;
        }
        AutorizedDomainPK other = (AutorizedDomainPK) object;
        if ((this.adDomainKey == null && other.adDomainKey != null) || (this.adDomainKey != null && !this.adDomainKey.equals(other.adDomainKey))) {
            return false;
        }
        if ((this.adDomainAccount == null && other.adDomainAccount != null) || (this.adDomainAccount != null && !this.adDomainAccount.equals(other.adDomainAccount))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.callsbox.domain.AutorizedDomainPK[ adDomainKey=" + adDomainKey + ", adDomainAccount=" + adDomainAccount + " ]";
    }
    
}
