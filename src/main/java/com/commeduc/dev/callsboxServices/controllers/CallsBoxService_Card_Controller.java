package com.commeduc.dev.callsboxServices.controllers;

import com.commeduc.dev.callsboxServices.models.Card;
import com.commeduc.dev.callsboxServices.models.CardDao;
import com.commeduc.dev.callsboxServices.models.User;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * Class UserController
 */
@Controller
public class CallsBoxService_Card_Controller {

  // ------------------------
  // PUBLIC METHODS
  // ------------------------

  /**
   * Create a new Card with an auto-generated id and code and amount as passed 
   * values.
   * The Date is just now .
   * 
     * @param code
     * @param amount
     * @return 
   */
  @RequestMapping(value="/create")
  @ResponseBody
  public String create(String code, double amount) {
    try {
      Card card = new Card(code, amount, new Date());
      cardDao.create(card);
    }
    catch (Exception ex) {
      return "Error creating the card: " + ex.toString();
    }
    return "Card succesfully created!";
  }
  
  /**
   * Delete the user with the passed id.
     * @param id
     * @return 
   */
  @RequestMapping(value="/delete")
  @ResponseBody
  public String delete(long id) {
    try {
      Card card = new Card(id);
      cardDao.delete(card);
    }
    catch (Exception ex) {
      return "Error deleting the Card: " + ex.toString();
    }
    return "Card succesfully deleted!";
  }
  
  /**
   * Retrieve the id for the card with the passed code.
     * @param code
     * @return 
   */
  @RequestMapping(value="/get-by-code")
  @ResponseBody
  public String getByCode(String code) {
    String cardId;
    try {
      Card card = cardDao.getByCode(code);
      cardId = String.valueOf(card.getId());
    }
    catch (Exception ex) {
      return "Card not found: " + ex.toString();
    }
    return "The card id is: " + cardId;
  }
  
  /**
   * Retrieve the id for the card with the passed code.
     * @param code
     * @return 
   */
  @RequestMapping(value="/get-owner-by-code")
  @ResponseBody
  public String getByOwnerByCode(String code) {
    String cardId;
    try {
      Card card = cardDao.getByCode(code);
      cardId = String.valueOf(card.getId());
      
      RestTemplate restTemplate = new RestTemplate();
      User user = restTemplate.getForObject("http://localhost:9091/get-by-id?id=1", User.class);
      
      // TODO
      
    }
    catch (Exception ex) {
      return "Card not found: " + ex.toString();
    }
    return "The card id is: " + cardId;
  }
  
  /**
   * Update the data for the Card identified by the passed id.
     * @param id
     * @param code
     * @param amount
     * @param expirationDate
     * @return 
   */
  @RequestMapping(value="/update")
  @ResponseBody
  public String updateName(long id, String code, double amount, Date expirationDate) {
    try {
      Card card = cardDao.getById(id);
      card.setCode(code);
      card.setAmount(amount);
      card.setExpirationDate(expirationDate);
      
      cardDao.update(card);
    }
    catch (Exception ex) {
      return "Error updating the user: " + ex.toString();
    }
    return "User succesfully updated!";
  } 

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // Wire the UserDao used inside this controller.
  @Autowired
  private CardDao cardDao;
  
} // class UserController
