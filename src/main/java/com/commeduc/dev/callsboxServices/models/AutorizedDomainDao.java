package com.commeduc.dev.callsboxServices.models;


import com.commeduc.dev.callsboxServices.domain.AutorizedDomain;
import com.commeduc.dev.callsboxServices.domain.CustomerAccount;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO wihtout any XML configuration and also provide the Spring 
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class AutorizedDomainDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the user in the database.
     * @param account
   */
  public void create(AutorizedDomain account) {
    entityManager.persist(account);
    return;
  }
  
  /**
   * Delete the user from the database.
     * @param account
   */
  public void delete(AutorizedDomain account) {
    if (entityManager.contains(account))
      entityManager.remove(account);
    else
      entityManager.remove(entityManager.merge(account));
    return;
  }
  
  /**
   * Return all the users stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<AutorizedDomain> getAll() {
    return entityManager.createQuery("from AutorizedDomain").getResultList();
  }
  
  /**
   * Return the user having the passed msisdn.
     * @param msisdn
     * @return 
   */
  public AutorizedDomain getByAdDomainKey(String key) {
    return (AutorizedDomain) entityManager.createQuery(
        "from AutorizedDomain where AD_DOMAIN_KEY = :adDomainKey")
        .setParameter("adDomainKey", key)
        .getSingleResult();
  }
  
  public AutorizedDomain getByAdDomainAccount(String account) {
    return (AutorizedDomain) entityManager.createQuery(
        "from AutorizedDomain where AD_DOMAIN_ACCOUNT = :adDomainAccount")
        .setParameter("adDomainAccount", account)
        .getSingleResult();
  }

  /**
   * Return the CustumerAccount having the passed id.
     * @param id
     * @return 
   */
  public AutorizedDomain getById(long id) {
    return entityManager.find(AutorizedDomain.class, id);
  }

  /**
   * Update the passed user in the database.
     * @param account
   */
  public void update(AutorizedDomain account) {
    entityManager.merge(account);
    return;
  }

  public CustomerAccount getAccountByCptCustId(String id_provider) throws NoResultException {
    return (CustomerAccount) entityManager.createQuery(
        "from CustomerAccount where CPT_CUST_ID = :cpTCustId")
        .setParameter("cpTCustId", id_provider)
        .getSingleResult();
  }
  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
