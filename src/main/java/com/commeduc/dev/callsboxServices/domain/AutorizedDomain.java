/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fonda
 */
@Entity
@Table(name = "autorized_domain")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AutorizedDomain.findAll", query = "SELECT a FROM AutorizedDomain a"),
    @NamedQuery(name = "AutorizedDomain.findByAdDomainKey", query = "SELECT a FROM AutorizedDomain a WHERE a.autorizedDomainPK.adDomainKey = :adDomainKey"),
    //@NamedQuery(name = "AutorizedDomain.findByAdDomainKey", query = "SELECT a FROM AutorizedDomain a WHERE a.adDomainKey = :adDomainKey"),
    @NamedQuery(name = "AutorizedDomain.findByAdDomainAccount", query = "SELECT a FROM AutorizedDomain a WHERE a.autorizedDomainPK.adDomainAccount = :adDomainAccount"),
    //@NamedQuery(name = "AutorizedDomain.findByAdDomainAccount", query = "SELECT a FROM AutorizedDomain a WHERE a.adDomainAccount = :adDomainAccount"),
    @NamedQuery(name = "AutorizedDomain.findByAdDomainDesc", query = "SELECT a FROM AutorizedDomain a WHERE a.adDomainDesc = :adDomainDesc"),
    @NamedQuery(name = "AutorizedDomain.findByAdDomainStatus", query = "SELECT a FROM AutorizedDomain a WHERE a.adDomainStatus = :adDomainStatus")})
public class AutorizedDomain implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AutorizedDomainPK autorizedDomainPK;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "AD_DOMAIN_DESC")
    private String adDomainDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AD_DOMAIN_STATUS")
    private boolean adDomainStatus;
  

    public AutorizedDomain() {
    }

    public AutorizedDomain(AutorizedDomainPK autorizedDomainPK) {
        this.autorizedDomainPK = autorizedDomainPK;
    }

    public AutorizedDomain(AutorizedDomainPK autorizedDomainPK, String adDomainDesc, boolean adDomainStatus) {
        this.autorizedDomainPK = autorizedDomainPK;
        this.adDomainDesc = adDomainDesc;
        this.adDomainStatus = adDomainStatus;
    }

    public AutorizedDomain(String adDomainKey, String adDomainAccount) {
        this.autorizedDomainPK = new AutorizedDomainPK(adDomainKey, adDomainAccount);
    }

    public AutorizedDomainPK getAutorizedDomainPK() {
        return autorizedDomainPK;
    }

    public void setAutorizedDomainPK(AutorizedDomainPK autorizedDomainPK) {
        this.autorizedDomainPK = autorizedDomainPK;
    }

    public String getAdDomainDesc() {
        return adDomainDesc;
    }

    public void setAdDomainDesc(String adDomainDesc) {
        this.adDomainDesc = adDomainDesc;
    }

    public boolean getAdDomainStatus() {
        return adDomainStatus;
    }

    public void setAdDomainStatus(boolean adDomainStatus) {
        this.adDomainStatus = adDomainStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autorizedDomainPK != null ? autorizedDomainPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutorizedDomain)) {
            return false;
        }
        AutorizedDomain other = (AutorizedDomain) object;
        if ((this.autorizedDomainPK == null && other.autorizedDomainPK != null) || (this.autorizedDomainPK != null && !this.autorizedDomainPK.equals(other.autorizedDomainPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.callsbox.domain.AutorizedDomain[ autorizedDomainPK=" + autorizedDomainPK + " ]";
    }
    
}
