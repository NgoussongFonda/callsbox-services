/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author deugueu
 */
@Entity
@Table(name = "res_lines")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ResourcesLine.findAll", query = "SELECT r FROM ResourcesLine r"),
    @NamedQuery(name = "ResourcesLine.findByResId", query = "SELECT r FROM ResourcesLine r WHERE r.resId = :resId"),
    @NamedQuery(name = "ResourcesLine.findByResNum", query = "SELECT r FROM ResourcesLine r WHERE r.resNum = :resNum"),
    @NamedQuery(name = "ResourcesLine.findByResSerialNumber", query = "SELECT r FROM ResourcesLine r WHERE r.resSerialNumber = :resSerialNumber"),
    @NamedQuery(name = "ResourcesLine.findByResImsi", query = "SELECT r FROM ResourcesLine r WHERE r.resImsi = :resImsi"),
    @NamedQuery(name = "ResourcesLine.findByResPinM", query = "SELECT r FROM ResourcesLine r WHERE r.resPinM = :resPinM"),
    @NamedQuery(name = "ResourcesLine.findByResPinStk", query = "SELECT r FROM ResourcesLine r WHERE r.resPinStk = :resPinStk"),
    @NamedQuery(name = "ResourcesLine.findByResPuk1", query = "SELECT r FROM ResourcesLine r WHERE r.resPuk1 = :resPuk1"),
    @NamedQuery(name = "ResourcesLine.findByResRatePlan", query = "SELECT r FROM ResourcesLine r WHERE r.resRatePlan = :resRatePlan"),
    @NamedQuery(name = "ResourcesLine.findByZoning", query = "SELECT r FROM ResourcesLine r WHERE r.zoning = :zoning"),
    @NamedQuery(name = "ResourcesLine.findByResHplmn", query = "SELECT r FROM ResourcesLine r WHERE r.resHplmn = :resHplmn"),
    @NamedQuery(name = "ResourcesLine.findByResUserType", query = "SELECT r FROM ResourcesLine r WHERE r.resUserType = :resUserType"),
    @NamedQuery(name = "ResourcesLine.findByResStatus", query = "SELECT r FROM ResourcesLine r WHERE r.resStatus = :resStatus"),
    @NamedQuery(name = "ResourcesLine.findByResPrtUse", query = "SELECT r FROM ResourcesLine r WHERE r.resPrtUse = :resPrtUse"),
    @NamedQuery(name = "ResourcesLine.findByResNotes", query = "SELECT r FROM ResourcesLine r WHERE r.resNotes = :resNotes"),
    @NamedQuery(name = "ResourcesLine.findByResTruncType", query = "SELECT r FROM ResourcesLine r WHERE r.resTruncType = :resTruncType"),
    @NamedQuery(name = "ResourcesLine.findByResTruncSup", query = "SELECT r FROM ResourcesLine r WHERE r.resTruncSup = :resTruncSup"),
    @NamedQuery(name = "ResourcesLine.findByResResetMs1", query = "SELECT r FROM ResourcesLine r WHERE r.resResetMs1 = :resResetMs1"),
    @NamedQuery(name = "ResourcesLine.findByResResetMs", query = "SELECT r FROM ResourcesLine r WHERE r.resResetMs = :resResetMs"),
    @NamedQuery(name = "ResourcesLine.findByResResetPin", query = "SELECT r FROM ResourcesLine r WHERE r.resResetPin = :resResetPin"),
    @NamedQuery(name = "ResourcesLine.findByResResetSms", query = "SELECT r FROM ResourcesLine r WHERE r.resResetSms = :resResetSms"),
    @NamedQuery(name = "ResourcesLine.findByResSendCall", query = "SELECT r FROM ResourcesLine r WHERE r.resSendCall = :resSendCall"),
    @NamedQuery(name = "ResourcesLine.findByResLastCheck", query = "SELECT r FROM ResourcesLine r WHERE r.resLastCheck = :resLastCheck"),
    @NamedQuery(name = "ResourcesLine.findByResCommand", query = "SELECT r FROM ResourcesLine r WHERE r.resCommand = :resCommand"),
    @NamedQuery(name = "ResourcesLine.findByResNom", query = "SELECT r FROM ResourcesLine r WHERE r.resNom = :resNom"),
    @NamedQuery(name = "ResourcesLine.findByResZone", query = "SELECT r FROM ResourcesLine r WHERE r.resZone = :resZone"),
    @NamedQuery(name = "ResourcesLine.findByResTransfType", query = "SELECT r FROM ResourcesLine r WHERE r.resTransfType = :resTransfType"),
    @NamedQuery(name = "ResourcesLine.findByResGlobalRecharge", query = "SELECT r FROM ResourcesLine r WHERE r.resGlobalRecharge = :resGlobalRecharge"),
    @NamedQuery(name = "ResourcesLine.findByResGlobalSell", query = "SELECT r FROM ResourcesLine r WHERE r.resGlobalSell = :resGlobalSell"),
    @NamedQuery(name = "ResourcesLine.findByResGlobalWithdraw", query = "SELECT r FROM ResourcesLine r WHERE r.resGlobalWithdraw = :resGlobalWithdraw")})
public class ResourcesLine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "RES_ID")
    private String resId;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_NUM")
    private String resNum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_SERIAL_NUMBER")
    private String resSerialNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_IMSI")
    private String resImsi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "RES_PIN_M")
    private String resPinM;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "RES_PIN_STK")
    private String resPinStk;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_PUK_1")
    private String resPuk1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "RES_RATE_PLAN")
    private String resRatePlan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Zoning")
    private int zoning;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_HPLMN")
    private String resHplmn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "RES_USER_TYPE")
    private String resUserType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "RES_STATUS")
    private String resStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_PRT_USE")
    private String resPrtUse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RES_NOTES")
    private String resNotes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "RES_TRUNC_TYPE")
    private String resTruncType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "RES_TRUNC_SUP")
    private String resTruncSup;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RES_RESET_MS_1")
    private String resResetMs1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RES_RESET_MS")
    private String resResetMs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RES_RESET_PIN")
    private String resResetPin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RES_RESET_SMS")
    private String resResetSms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RES_SEND_CALL")
    private String resSendCall;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RES_LAST_CHECK")
    private String resLastCheck;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RES_COMMAND")
    private int resCommand;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "RES_NOM")
    private String resNom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "RES_ZONE")
    private String resZone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "RES_TRANSF_TYPE")
    private String resTransfType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RES_GLOBAL_RECHARGE")
    private double resGlobalRecharge;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RES_GLOBAL_SELL")
    private double resGlobalSell;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RES_GLOBAL_WITHDRAW")
    private double resGlobalWithdraw;

    public ResourcesLine() {
    }

    public ResourcesLine(String resNum) {
        this.resNum = resNum;
    }

    public ResourcesLine(String resNum, String resId, String resSerialNumber, String resImsi, String resPinM, String resPinStk, String resPuk1, String resRatePlan, int zoning, String resHplmn, String resUserType, String resStatus, String resPrtUse, String resNotes, String resTruncType, String resTruncSup, String resResetMs1, String resResetMs, String resResetPin, String resResetSms, String resSendCall, String resLastCheck, int resCommand, String resNom, String resZone, String resTransfType, double resGlobalRecharge, double resGlobalSell, double resGlobalWithdraw) {
        this.resNum = resNum;
        this.resId = resId;
        this.resSerialNumber = resSerialNumber;
        this.resImsi = resImsi;
        this.resPinM = resPinM;
        this.resPinStk = resPinStk;
        this.resPuk1 = resPuk1;
        this.resRatePlan = resRatePlan;
        this.zoning = zoning;
        this.resHplmn = resHplmn;
        this.resUserType = resUserType;
        this.resStatus = resStatus;
        this.resPrtUse = resPrtUse;
        this.resNotes = resNotes;
        this.resTruncType = resTruncType;
        this.resTruncSup = resTruncSup;
        this.resResetMs1 = resResetMs1;
        this.resResetMs = resResetMs;
        this.resResetPin = resResetPin;
        this.resResetSms = resResetSms;
        this.resSendCall = resSendCall;
        this.resLastCheck = resLastCheck;
        this.resCommand = resCommand;
        this.resNom = resNom;
        this.resZone = resZone;
        this.resTransfType = resTransfType;
        this.resGlobalRecharge = resGlobalRecharge;
        this.resGlobalSell = resGlobalSell;
        this.resGlobalWithdraw = resGlobalWithdraw;
    }

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public String getResNum() {
        return resNum;
    }

    public void setResNum(String resNum) {
        this.resNum = resNum;
    }

    public String getResSerialNumber() {
        return resSerialNumber;
    }

    public void setResSerialNumber(String resSerialNumber) {
        this.resSerialNumber = resSerialNumber;
    }

    public String getResImsi() {
        return resImsi;
    }

    public void setResImsi(String resImsi) {
        this.resImsi = resImsi;
    }

    public String getResPinM() {
        return resPinM;
    }

    public void setResPinM(String resPinM) {
        this.resPinM = resPinM;
    }

    public String getResPinStk() {
        return resPinStk;
    }

    public void setResPinStk(String resPinStk) {
        this.resPinStk = resPinStk;
    }

    public String getResPuk1() {
        return resPuk1;
    }

    public void setResPuk1(String resPuk1) {
        this.resPuk1 = resPuk1;
    }

    public String getResRatePlan() {
        return resRatePlan;
    }

    public void setResRatePlan(String resRatePlan) {
        this.resRatePlan = resRatePlan;
    }

    public int getZoning() {
        return zoning;
    }

    public void setZoning(int zoning) {
        this.zoning = zoning;
    }

    public String getResHplmn() {
        return resHplmn;
    }

    public void setResHplmn(String resHplmn) {
        this.resHplmn = resHplmn;
    }

    public String getResUserType() {
        return resUserType;
    }

    public void setResUserType(String resUserType) {
        this.resUserType = resUserType;
    }

    public String getResStatus() {
        return resStatus;
    }

    public void setResStatus(String resStatus) {
        this.resStatus = resStatus;
    }

    public String getResPrtUse() {
        return resPrtUse;
    }

    public void setResPrtUse(String resPrtUse) {
        this.resPrtUse = resPrtUse;
    }

    public String getResNotes() {
        return resNotes;
    }

    public void setResNotes(String resNotes) {
        this.resNotes = resNotes;
    }

    public String getResTruncType() {
        return resTruncType;
    }

    public void setResTruncType(String resTruncType) {
        this.resTruncType = resTruncType;
    }

    public String getResTruncSup() {
        return resTruncSup;
    }

    public void setResTruncSup(String resTruncSup) {
        this.resTruncSup = resTruncSup;
    }

    public String getResResetMs1() {
        return resResetMs1;
    }

    public void setResResetMs1(String resResetMs1) {
        this.resResetMs1 = resResetMs1;
    }

    public String getResResetMs() {
        return resResetMs;
    }

    public void setResResetMs(String resResetMs) {
        this.resResetMs = resResetMs;
    }

    public String getResResetPin() {
        return resResetPin;
    }

    public void setResResetPin(String resResetPin) {
        this.resResetPin = resResetPin;
    }

    public String getResResetSms() {
        return resResetSms;
    }

    public void setResResetSms(String resResetSms) {
        this.resResetSms = resResetSms;
    }

    public String getResSendCall() {
        return resSendCall;
    }

    public void setResSendCall(String resSendCall) {
        this.resSendCall = resSendCall;
    }

    public String getResLastCheck() {
        return resLastCheck;
    }

    public void setResLastCheck(String resLastCheck) {
        this.resLastCheck = resLastCheck;
    }

    public int getResCommand() {
        return resCommand;
    }

    public void setResCommand(int resCommand) {
        this.resCommand = resCommand;
    }

    public String getResNom() {
        return resNom;
    }

    public void setResNom(String resNom) {
        this.resNom = resNom;
    }

    public String getResZone() {
        return resZone;
    }

    public void setResZone(String resZone) {
        this.resZone = resZone;
    }

    public String getResTransfType() {
        return resTransfType;
    }

    public void setResTransfType(String resTransfType) {
        this.resTransfType = resTransfType;
    }

    public double getResGlobalRecharge() {
        return resGlobalRecharge;
    }

    public void setResGlobalRecharge(double resGlobalRecharge) {
        this.resGlobalRecharge = resGlobalRecharge;
    }

    public double getResGlobalSell() {
        return resGlobalSell;
    }

    public void setResGlobalSell(double resGlobalSell) {
        this.resGlobalSell = resGlobalSell;
    }

    public double getResGlobalWithdraw() {
        return resGlobalWithdraw;
    }

    public void setResGlobalWithdraw(double resGlobalWithdraw) {
        this.resGlobalWithdraw = resGlobalWithdraw;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resNum != null ? resNum.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResourcesLine)) {
            return false;
        }
        ResourcesLine other = (ResourcesLine) object;
        if ((this.resNum == null && other.resNum != null) || (this.resNum != null && !this.resNum.equals(other.resNum))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.commeduc.dev.callsbox.common.ResourcesLine[ resNum=" + resNum + " ]";
    }
    
}
