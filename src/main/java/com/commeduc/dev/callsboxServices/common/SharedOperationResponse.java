/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.common;

/**
 *
 * @author deugueu
 */
public class SharedOperationResponse {
    
    private boolean success, valid;
    private int credit;
    private String message;

    public SharedOperationResponse(boolean success, boolean valid, int credit, String message) {
        this.success = success;
        this.valid = valid;
        this.credit = credit;
        this.message = message;
    }

    
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
    
    
}
