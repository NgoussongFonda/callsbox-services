package com.commeduc.dev.callsboxServices.models;


import com.commeduc.dev.callsboxServices.domain.CustomerAccount;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO wihtout any XML configuration and also provide the Spring 
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class CustomerAccountDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the user in the database.
     * @param account
   */
  public void create(CustomerAccount account) {
    entityManager.persist(account);
    return;
  }
  
  /**
   * Delete the user from the database.
     * @param account
   */
  public void delete(CustomerAccount account) {
    if (entityManager.contains(account))
      entityManager.remove(account);
    else
      entityManager.remove(entityManager.merge(account));
    return;
  }
  
  /**
   * Return all the users stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<CustomerAccount> getAll() {
    return entityManager.createQuery("from CustomerAccount").getResultList();
  }
  
  /**
   * Return the user having the passed msisdn.
     * @param msisdn
     * @return 
   */
  public CustomerAccount getByMsIsdn(String msisdn) {
    return (CustomerAccount) entityManager.createQuery(
        "from CustomerAccount where CPT_ISDN = :msisdn")
        .setParameter("msisdn", msisdn)
        .getSingleResult();
  }
  
  public CustomerAccount getBycptcustid(String msisdn) {
    return (CustomerAccount) entityManager.createQuery(
        "from CustomerAccount where replace(CPT_CUST_ID,'.','') = :msisdn")
        .setParameter("msisdn", msisdn)
        .getSingleResult();
  }

  /**
   * Return the CustumerAccount having the passed id.
     * @param id
     * @return 
   */
  public CustomerAccount getById(long id) {
    return entityManager.find(CustomerAccount.class, id);
  }

  /**
   * Update the passed user in the database.
     * @param account
   */
  public void update(CustomerAccount account) {
    entityManager.merge(account);
    return;
  }

  public CustomerAccount getAccountByCptCustId(String id_provider) throws NoResultException {
    return (CustomerAccount) entityManager.createQuery(
        "from CustomerAccount where CPT_CUST_ID = :cpTCustId")
        .setParameter("cpTCustId", id_provider)
        .getSingleResult();
  }
  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
