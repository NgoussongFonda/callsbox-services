/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.controllers;
import com.commeduc.dev.callsboxServices.models.CustomerAccountDao;
import com.commeduc.dev.callsboxServices.models.ResourcesLineDao;
import com.commeduc.dev.callsboxServices.common.ProviderNumbersUtils;
import com.commeduc.dev.callsboxServices.common.SharedOperationResponse;
import com.commeduc.dev.callsboxServices.common.StringConsts;
import com.commeduc.dev.callsboxServices.domain.CustomerAccount;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author deugueu
 */
@Controller
public class Apk_Service_Conversion_TopUpController 
{
    // Wire the UserDao used inside this controller.
    @Autowired
    private ResourcesLineDao resourcesLineDao;
    // Wire the UserDao used inside this controller.
    @Autowired
    private CustomerAccountDao custumerAccountDao;


    /**
     * @param UsingService
     * @param BasicService
     * @param sender_msisdn
     * @param Type_Conversion
     * @param imei
     * @param imsi
     * @param receiver_msisdn
     * @param amount
     * @param callbacklocation
     * @return
     */

    @RequestMapping(value="/callsbox/services/conversion/{UsingService}/{sender_msisdn}/{Type_Conversion}/{imei}/{imsi}/{callbacklocation}/{BasicService}/{receiver_msisdn}/{amount}", method = {RequestMethod.GET},
        /*consumes = MediaType.APPLICATION_XML_VALUE,*/
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public SharedOperationResponse CallsBoxServiceGsmRequest(
            @PathVariable(value="sender_msisdn") String sender_msisdn,
            @PathVariable(value="Type_Conversion") String Type_Conversion,
            @PathVariable(value="imei") String imei,
            @PathVariable(value="imsi") String imsi,
            @PathVariable(value="callbacklocation") String callbacklocation,
            @PathVariable("UsingService") int UsingService,
            @PathVariable("BasicService") int BasicService,
            @PathVariable(value="receiver_msisdn") String receiver_msisdn,
            @PathVariable(value="amount") String amount ) 
    {
            // Vérifier la validité du service
            
            String typeCommand = "" ;
            String ServiceSelected = "" ;
            ServiceSelected="SERVICE_"+BasicService;
            if(""+UsingService+""+BasicService == "StringConsts."+ServiceSelected){
                typeCommand = "I"+UsingService+"*S"+BasicService+"*"+Type_Conversion;
            }
            else {
                return new SharedOperationResponse(false,false,-1, "Le Service sollicité n'existe pas. Merci de vérifier vos paramètres");
            }
            try {
                CustomerAccount SenderAccount = custumerAccountDao.getByMsIsdn(sender_msisdn);
                if(SenderAccount.getCptAccountStatus().equals(0)){                   
                  return new SharedOperationResponse(false,false,-1, "Votre Commpte n'est pas encore activé.");
                }

                // Controle de la confirmité du numéro recepteur
                
                String provider = ProviderNumbersUtils.getProvider(receiver_msisdn);

                 // Preparation à l'exécution de la demande
                    Date currenTime = Calendar.getInstance().getTime();
                    DateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
                    File deposit = new File(StringConsts.FILE_DEPOSIT);
                    File requestFile = new File(deposit, "CDRAPK"+sender_msisdn+""+format.format(currenTime)+".dat" );
                    String executionCommand = "";
                    executionCommand = MainController.getConversionExecutionCommand(sender_msisdn, receiver_msisdn, amount, imei, imsi, callbacklocation,typeCommand, requestFile);
                    try 
                    {
                        //Path creation = deposit.toPath().resolve( requestFile.getName() );
                        Path created = Files.createFile(requestFile.toPath() );
                        Files.write(
                                Paths.get(requestFile.toURI()), 
                                executionCommand.getBytes("utf-8"), 
                                StandardOpenOption.CREATE, 
                                StandardOpenOption.TRUNCATE_EXISTING);
                                Logger.getLogger(Apk_Service_Conversion_TopUpController.class.getName()).log(Level.INFO, "File created and stored : "+created.toString(), created);
                    } catch (IOException ex) {
                        Logger.getLogger(Apk_Service_Conversion_TopUpController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return new SharedOperationResponse(true, true, 18000, "Votre Demande a été prise en Compte.Vous allez recevoir un SMS de confirmation");
                

            }
            catch(org.springframework.dao.EmptyResultDataAccessException | javax.persistence.NoResultException nre){
                return new SharedOperationResponse(false,false,-10, "No user found with this username.");
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }return new SharedOperationResponse(false,false,-10, "Something went wrong. Retry");
    }

}
