package com.commeduc.dev.callsboxServices.models;

import com.commeduc.dev.callsboxServices.domain.Easycard;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

/**
 * This class is used to access data for the User entity.
 * Repository annotation allows the component scanning support to find and 
 * configure the DAO wihtout any XML configuration and also provide the Spring 
 * exceptiom translation.
 * Since we've setup setPackagesToScan and transaction manager on
 * DatabaseConfig, any bean method annotated with Transactional will cause
 * Spring to magically call begin() and commit() at the start/end of the
 * method. If exception occurs it will also call rollback().
 */
@Repository
@Transactional
public class EasycardDao {
  
  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  /**
   * Save the user in the database.
     * @param card
   */
  public void create(Easycard card) {
    entityManager.persist(card);
    return;
  }
  
  /**
   * Delete the user from the database.
     * @param card
   */
  public void delete(Easycard card) {
    if (entityManager.contains(card))
      entityManager.remove(card);
    else
      entityManager.remove(entityManager.merge(card));
    return;
  }
  
  /**
   * Return all the users stored in the database.
     * @return 
   */
  @SuppressWarnings("unchecked")
  public List<Easycard> getAll() {
    return entityManager.createQuery("from Easycard").getResultList();
  }
  
  /**
   * Return the user having the passed email.
     * @param serial
     * @return an EasyCard retrieved from its serial
   */
  public Easycard getBySerial(String serial) {
    return (Easycard) entityManager.createQuery(
        "FROM Easycard e WHERE e.eCSerial LIKE :eCSerial")
        .setParameter("eCSerial", serial)
        .getSingleResult();
  }
  
    /**
   * Return the user having the passed email.
     * @param cardCode
     * @return 
   */
  public Easycard getByCode(String cardCode) {
    Easycard card;
    try {
        card = (Easycard) entityManager.createQuery(
        "FROM Easycard e WHERE e.eCPwD LIKE :eCPwD")
        .setParameter("eCPwD", cardCode)
        .getSingleResult();
    }
    catch(javax.persistence.NoResultException exception){
        System.out.println("Il ne trouve rien" + exception.getMessage() );
        return null;
    }

    return card;

  }  


  /**
   * Update the passed user in the database.
     * @param card
   */
  public void update(Easycard card) {
    entityManager.merge(card);
    return;
  }

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  // An EntityManager will be automatically injected from entityManagerFactory
  // setup on DatabaseConfig class.
  @PersistenceContext
  private EntityManager entityManager;
  
} // class UserDao
