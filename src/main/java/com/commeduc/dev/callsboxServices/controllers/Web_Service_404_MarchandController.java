/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.commeduc.dev.callsboxServices.controllers;


import com.commeduc.dev.callsboxServices.models.AutorizedDomainDao;


import com.commeduc.dev.callsboxServices.models.CustomerAccountDao;
import com.commeduc.dev.callsboxServices.models.ResourcesLineDao;
import com.commeduc.dev.callsboxServices.common.ProviderNumbersUtils;
import com.commeduc.dev.callsboxServices.common.SharedCustomerAccount;
import com.commeduc.dev.callsboxServices.common.SharedOperationResponse;
import com.commeduc.dev.callsboxServices.common.StringConsts;
import com.commeduc.dev.callsboxServices.domain.AutorizedDomain;
import com.commeduc.dev.callsboxServices.domain.CustomerAccount;
import com.commeduc.dev.callsboxServices.domain.ResourcesLine;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author deugueu
 */
@Controller
public class Web_Service_404_MarchandController {
    
    // Wire the UserDao used inside this controller.
  @Autowired
  private ResourcesLineDao resourcesLineDao;
   // Wire the UserDao used inside this controller.
  @Autowired
  private CustomerAccountDao custumerAccountDao;
  @Autowired
  private AutorizedDomainDao autorizedDomainDao;
  

    /**
     * Registration - Application sends Sender_msisdn, password, profile, Sender_msisdn and share.
     *                      receives a JSON Representation of an AccountObject. 
     * 
     * @param id_provider
     * @param id_customer_marchand
     * @param Sender_msisdn
     * @param share
     * @param amount
     * @param callback (Kaolo)
     * @param id_transaction
     * @return 
     */
    @RequestMapping(value="/callsbox/services/webmarchand/{UsingService}/{BasicService}/{id_provider}/{id_customer_marchand}/{Sender_msisdn}/{share}/{amount}/{callback}/{id_transaction}", method = {RequestMethod.GET},
   /*consumes = MediaType.APPLICATION_XML_VALUE,*/
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public SharedOperationResponse callsBoxConversion(

            @PathVariable(value="UsingService") String UsingService,
            @PathVariable(value="BasicService") String BasicService,
            
            @PathVariable(value="id_provider") String id_provider,
            @PathVariable(value="id_customer_marchand") String id_customer_marchand,
            
            @PathVariable(value="Sender_msisdn") String Sender_msisdn,
            @PathVariable(value="share") int share,
            @PathVariable(value="callback") String callback,
            
            @PathVariable(value="id_transaction") String id_transaction,
            @PathVariable(value="amount") String amount, HttpServletRequest request) {
        
        String imei = "";
        String imsi = "";
        String typeCommand = "" ;
        String ServiceSelected = "" ;
        ServiceSelected="SERVICE_"+BasicService;
        String join = "404203";
        if(join == StringConsts.SERVICE_404203){
            typeCommand = "I"+UsingService+"*S"+BasicService;
            
            
        }
        
        else {
            System.out.println(UsingService+""+BasicService +" == "+StringConsts.SERVICE_404203);
            return new SharedOperationResponse(false,false,-1, "Le Service sollicité n'existe pas. Merci de vérifier vos paramètres");
        }
        // make a control on provider ID ( compare the token with the database)
         try {
            
                // Make a control on the customer marchand ID
                CustomerAccount compteClient = custumerAccountDao.getAccountByCptCustId(id_provider);
                
                //Make a control on the account id (that is, provider id from autorized class)
                AutorizedDomain compte = autorizedDomainDao.getByAdDomainAccount(id_provider);
                
                
                Logger.getLogger(Web_Service_404_MarchandController.class.getName()).log(Level.INFO, "\nGET Request for "+ Sender_msisdn + " : "+compteClient.getCptPwd()
                            + " VS "+id_customer_marchand, Sender_msisdn);
                
                
                //String ip = InetAddress.getLocalHost().getHostAddress();
                String hostname = request.getRemoteAddr(); 
                System.out.println("THIS IS THE HOSTNAME>>>>>>>>>>>>>>>>>>"+hostname);
                
                /*Remove "." from id_customer_marchand, concat with hostname and hash*/
               
                String code = id_provider + hostname; //"kaolo.commeduc.com";
                String sha256hex = DigestUtils.md5DigestAsHex(code.getBytes());
                //String sha3_256hex = new DigestUtils(SHA3_256).digestAsHex(code);
                //byte[] pwd = DigestUtils.md5Digest(code.getBytes());
                
                System.out.println("THIS IS THE ACCOUNT >>>>>>>>>>>>>>>>>>"+compte.getAutorizedDomainPK().getAdDomainAccount());
                System.out.println("THIS IS THE TOKEN>>>>>>>>>>>>>>>>>>"+sha256hex);
                
                
                if(!compte.getAutorizedDomainPK().getAdDomainAccount().equals(id_provider)){                   
                    return new SharedOperationResponse(false,false,-1, "Wrong code. Try again.");
                }
                else{
                
                    
                    if(compteClient.getCptAccountStatus().equals("N")){                   
                        return new SharedOperationResponse(false,false,-1, "Votre Commpte n'est pas encore activé.");
                    }
                
                    else {
                    
                        CustomerAccount marchand = custumerAccountDao.getAccountByCptCustId(id_customer_marchand);
                        if(!marchand.getCptCustId().equals(id_customer_marchand)){                   
                            return new SharedOperationResponse(false,false,-1, "Wrong code. Try again.");
                           
                        }                       
                        
                        String provider = ProviderNumbersUtils.getProvider(id_provider);
                        
                        //ResourcesLine line = resourcesLineDao.getByTrunkSupAndHplmn(conversionType, provider);

                        Date currenTime = Calendar.getInstance().getTime();
                        DateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
                        File deposit = new File(StringConsts.FILE_DEPOSIT);
                        File requestFile = new File(deposit, "CDRAPK"+id_provider.replace(".", "")+""+format.format(currenTime)+".dat" );
                        //String typeCommand = "I200*S200";
                    
                        String executionCommand = "";
                            executionCommand = MainController.getWithdrawWebMarchandExecutionCommand(id_customer_marchand, 
                            Sender_msisdn, imei, imsi, callback, share, id_transaction, amount, typeCommand, requestFile);
                            System.out.print("TOTODINHO LA LEGENDE <<<<<<<<<<<<<<");
                            
                            try {
                            //Path creation = deposit.toPath().resolve( requestFile.getName() );
                            Path created = Files.createFile(requestFile.toPath() );
                            Files.write(
                                Paths.get(requestFile.toURI()), 
                                executionCommand.getBytes("utf-8"), 
                                StandardOpenOption.CREATE, 
                                StandardOpenOption.TRUNCATE_EXISTING);
                        
                            System.out.print("TOTODINHO LA LEGENDE <<<<<<<<<<<<<<");

                            Logger.getLogger(Web_Service_404_MarchandController.class.getName()).log(Level.INFO, "File created and stored : "+created.toString(), created);


                        } catch (IOException ex) {
                            Logger.getLogger(Web_Service_404_MarchandController.class.getName()).log(Level.SEVERE, null, ex);
                        }


                        Logger.getLogger(Web_Service_404_MarchandController.class.getName()).log(Level.INFO, "GET Request for "+ Sender_msisdn + " just landed : LINE "+ "arthur", Sender_msisdn);
                        return new SharedOperationResponse(true, true, 18000, "Votre Demande a été prise en Compte");
                    
                    }
                }                
                
            }
            catch(org.springframework.dao.EmptyResultDataAccessException | javax.persistence.NoResultException nre){
                return new SharedOperationResponse(false,false,-10, "No user found with this username.");
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        
        return new SharedOperationResponse(false,false,-10, "Something went wrong. Retry");
    }


}
